// Tools
import { FUNCTIONS } from './functions';

describe('FUNCTIONS', () => {

    it('should return "gray"', () => {
        const _color: string = FUNCTIONS.getClassByNumber();
        expect(_color).toEqual('gray');
    });

    it('should return "gray"', () => {
        const _color: string = FUNCTIONS.getClassByNumber(0);
        expect(_color).toEqual('gray');
    });

    it('should return "yellow"', () => {
        const _color: string = FUNCTIONS.getClassByNumber(5);
        expect(_color).toEqual('yellow');
    });
});