// Function to get class (string) for DOM element by input type number
export class FUNCTIONS {
    static getClassByNumber(index?: number): string {
        if (index === 1 || index === 4 || index === 7 || index === 10) {
            return 'red'
        } else if (index === 2 || index === 5 || index === 8) {
            return 'yellow'
        } else if (index === 3 || index === 6 || index === 9) {
            return 'green';
        }
        return 'gray';
    }
}