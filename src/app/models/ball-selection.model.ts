export interface BallSelection {
    item: string;
    selected: boolean;
}