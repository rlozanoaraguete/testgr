// Angular
import { Injectable } from '@angular/core';
// Pipes
import { SelectedPipe } from '../pipes/selected.pipe';
// Models
import { BallSelection } from '../models/ball-selection.model';
// Vendor
import { Observable, of, delay, BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BetService {

  public ballNumberEvent: BehaviorSubject<Array<BallSelection>>;
  public ballNumberEvent$: Observable<any>;
  public betDoneEvent: Subject<any>;
  public betDoneEvent$: Observable<any>;
  public totalEvent: Subject<any>;
  public totalEvent$: Observable<any>;
  public selectedBalls: Array<BallSelection>;
  public min: number;
  public max: number;
  public betting: boolean;
  public winningBet: string;

  private _selectedPipe: SelectedPipe;

  constructor() {
    this.ballNumberEvent = new BehaviorSubject<Array<BallSelection>>([]);
    this.ballNumberEvent$ = this.ballNumberEvent.asObservable();
    this.betDoneEvent = new Subject();
    this.betDoneEvent$ = this.betDoneEvent.asObservable();
    this.totalEvent = new Subject();
    this.totalEvent$ = this.totalEvent.asObservable();
    this.selectedBalls = [];
    this.min = 1;
    this.max = 10;
    this.betting = false;
    this.winningBet = '';
    this._initBallNumbers();

    this._selectedPipe = new SelectedPipe();
  }

  public init(): void {
    this.betting = false;
    this.winningBet = '';
    this.setBetDoneEvent(false);
    this._initBallNumbers();
  }

  public setBallNumber(event: BallSelection): void {
    const _ballNumbers: Array<BallSelection> = this.ballNumberEvent.getValue();
    this.selectedBalls = this._selectedPipe.transform(_ballNumbers);
    if (this.selectedBalls.length < 8 || _ballNumbers[+event.item - 1].selected) {
      _ballNumbers[+event.item - 1].selected = !_ballNumbers[+event.item - 1].selected;
      this.selectedBalls = this._selectedPipe.transform(_ballNumbers);
    }
    this.ballNumberEvent.next([..._ballNumbers]);
  }

  public clear(): void {
    this._initBallNumbers();
  }

  public placeBet(): Observable<string> {
    this.betting = true;
    this.winningBet = '';
    const _winningBet: number = Math.floor(Math.random() * (this.max - this.min + 1) + this.min);
    return of(_winningBet.toString()).pipe(delay(2000));
  }

  public getBetDoneEvent(): Observable<boolean> {
    return this.betDoneEvent$;
  }

  public setBetDoneEvent(event: boolean): void {
    this.betDoneEvent.next(event);
  }

  public getTotalEvent(): Observable<number> {
    return this.totalEvent$;
  }

  public setTotalEvent(event: number): void {
    this.totalEvent.next(event);
  }

  private _initBallNumbers(): void {
    const _ballsSelectionInitial: Array<BallSelection> = [];
    for (let i = 0; i < this.max; i++) {
      _ballsSelectionInitial.push({ item: (i + 1).toString(), selected: false });
    }
    this.ballNumberEvent.next(_ballsSelectionInitial);
  }

}
