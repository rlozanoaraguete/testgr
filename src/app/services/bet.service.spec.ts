// Services
import { BetService } from './bet.service';
// Pipes
import { SelectedPipe } from '../pipes/selected.pipe';
// Vendor
import { SpectatorService, createServiceFactory } from '@ngneat/spectator';


describe('BetService', () => {
  let spectator: SpectatorService<BetService>;

  const createService = createServiceFactory(
    {
      service: BetService,
      declarations: [SelectedPipe],
      mocks: [],
      providers: [
      ]
    });

  beforeEach(() => {
    spectator = createService();
  });

  it('should pass', () => {
    expect(true).toBeTruthy();
  });

  it('should exist', () => {
    expect(spectator.service).toBeDefined();
  });

  it('should set selectedBalls like empty array', () => {
    spectator.service.init();
    expect(spectator.service.selectedBalls).toEqual([]);
  });

  it('should set selectedBalls like empty array', () => {
    spectator.service.clear();
    expect(spectator.service.selectedBalls).toEqual([]);
  });

});