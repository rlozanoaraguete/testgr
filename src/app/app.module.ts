// Angular
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
// Components
import { AppComponent } from './app.component';
import { BallSelectorComponent } from './components/ball-selector.component/ball-selector.component';
import { BetSlipComponent } from './components/bet-slip.component/bet-slip.component';
// Pipes
import { SelectedPipe } from './pipes/selected.pipe';


@NgModule({
  declarations: [
    AppComponent,
    BallSelectorComponent,
    BetSlipComponent,
    SelectedPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
