// Components
import { AppComponent } from './app.component';
// Vendor
import { Spectator, createComponentFactory } from '@ngneat/spectator';

describe('AppComponent', () => {
  let spectator: Spectator<AppComponent>;
  const createComponent = createComponentFactory({
    component: AppComponent,
    detectChanges: false,
    mocks: [],
    imports: [],
    providers: [],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should pass', () => {
    expect(true).toBeTruthy();
  });

  it('should exist', () => {
    expect(spectator.component).toBeDefined();
  });

  it('should render just one component "app-ball-selector"', () => {
    expect(spectator.queryAll('app-ball-selector')).toHaveLength(1);
  });

  it('should render just one component "app-bet-slip"', () => {
    expect(spectator.queryAll('app-bet-slip')).toHaveLength(1);
  });

});
