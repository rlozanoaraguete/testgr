// Pipes
import { SelectedPipe } from './selected.pipe';
// Models
import { BallSelection } from '../models/ball-selection.model';

describe('Selected', () => {

    it('should return empty array', () => {
        const _pipe: SelectedPipe = new SelectedPipe();
        const _balls: Array<BallSelection> = [];
        expect(_pipe.transform(_balls)).toEqual([]);
    });

    it('should return empty array', () => {
        const _pipe: SelectedPipe = new SelectedPipe();
        const _balls: Array<BallSelection> = [
            {
                item: '1',
                selected: false
            },
            {
                item: '2',
                selected: false
            }
        ];
        expect(_pipe.transform(_balls)).toEqual([]);
    });

    it('should return array with selected items', () => {
        const _pipe: SelectedPipe = new SelectedPipe();
        const _balls: Array<BallSelection> = [
            {
                item: '1',
                selected: false
            },
            {
                item: '2',
                selected: true
            }
        ];
        expect(_pipe.transform(_balls)).toEqual([{item: '2', selected: true}]);
    });
});