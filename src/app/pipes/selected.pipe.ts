// Angular
import { Pipe, PipeTransform } from '@angular/core';
// Models
import { BallSelection } from '../models/ball-selection.model';

@Pipe({ name: 'selected' })
export class SelectedPipe implements PipeTransform {
  transform(input: Array<BallSelection>): any {
    const _selected: Array<BallSelection> = input.filter((item: BallSelection) => item.selected);
    return _selected || [];
  }
}
