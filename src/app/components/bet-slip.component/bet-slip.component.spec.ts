// Angular
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// Components
import { BetSlipComponent } from './bet-slip.component';
// Pipes
import { SelectedPipe } from 'src/app/pipes/selected.pipe';
// Vendor
import { Spectator, createComponentFactory } from '@ngneat/spectator';

describe('BetSlipComponent', () => {
  let spectator: Spectator<BetSlipComponent>;
  const createComponent = createComponentFactory({
    component: BetSlipComponent,
    declarations: [SelectedPipe],
    detectChanges: false,
    mocks: [],
    imports: [],
    providers: [FormBuilder]
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should pass', () => {
    expect(true).toBeTruthy();
  });

  it('should exist', () => {
    expect(spectator.component).toBeDefined();
  });

  it('should get "red" class', () => {
    expect(spectator.component.getClass('1')).toEqual('red');
  });

});
