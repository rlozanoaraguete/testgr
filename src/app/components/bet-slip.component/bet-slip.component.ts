// Angular
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Services
import { BetService } from 'src/app/services/bet.service';
// Pipes
import { SelectedPipe } from 'src/app/pipes/selected.pipe';
// Models
import { BallSelection } from 'src/app/models/ball-selection.model';
// Tools
import { FUNCTIONS } from 'src/app/tools/functions';

@Component({
  selector: 'app-bet-slip',
  templateUrl: 'bet-slip.component.html'
})
export class BetSlipComponent implements OnInit {

  public betAmountForm: FormGroup;
  public ballNumbers: Array<BallSelection>;
  public total: number;

  private _selectedPipe: SelectedPipe;

  constructor(
    public betService: BetService,
    private _fb: FormBuilder) {
    this.betAmountForm = this._fb.group({
      betAmount: [0, [Validators.required, Validators.pattern('^[0-9]{1,}$'), Validators.min(5)]]
    });
    this.ballNumbers = [];
    this.total = 0;

    this._selectedPipe = new SelectedPipe();
  }

  public ngOnInit(): void {
    this.betService.ballNumberEvent$.subscribe((res: Array<BallSelection>) => {
      const _selectedBalls: Array<BallSelection> = this._selectedPipe.transform(res);
      if (!_selectedBalls.length) {
        this._init();
      } else {
        this.ballNumbers = [..._selectedBalls];
        for (let i = 0; i < 8 - _selectedBalls.length; i++) {
          const _ballSelection: BallSelection = {
            item: '',
            selected: false
          }
          this.ballNumbers.push(_ballSelection);
        }
      }
    });
  }

  public setTotal(): void {
    if (!this.submitDisabled) {
      this.total = this.betAmountForm.get('betAmount')?.value * this.betService.selectedBalls.length;
    }
  }

  public placeBet(): void {
    if (this.total && !this.betService.betting) {
      this.betService.placeBet().subscribe((res: any) => {
        this.betService.betting = false;
        this.betService.winningBet = res;
        this.betService.setTotalEvent(this.total);
        this.betService.setBetDoneEvent(true);
      });
    }
  }

  public getClass(index: string): string {
    return FUNCTIONS.getClassByNumber(+index);
  }

  get submitDisabled(): boolean {
    return this.betAmountForm.invalid || this.betService.betting || !this._selectedPipe.transform(this.betService.selectedBalls).length;
  }

  get errorMessage(): string {
    if (isNaN(this.betAmountForm.get('betAmount')?.value)) {
      return 'Incorrect format, type a numeric value';
    } else if (this.betAmountForm.get('betAmount')?.value && this.betAmountForm.get('betAmount')?.value < 5) {
      return 'Please, type a value greater than 5';
    } else if (!this.betAmountForm.get('betAmount')?.value) {
      return 'Amount: mandatory value';
    }
    return '';
  }

  private _init(): void {
    this.betAmountForm.get('betAmount')?.setValue(0);
    this.ballNumbers = [];
    this.total = 0;
    this.betService.setTotalEvent(0);
    this.betService.setBetDoneEvent(false);
    for (let i = 0; i < 8; i++) {
      const _ballSelection: BallSelection = {
        item: '',
        selected: false
      }
      this.ballNumbers.push(_ballSelection);
    }
  }

}
