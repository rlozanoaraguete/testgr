// Angular
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// Services
import { BetService } from 'src/app/services/bet.service';
// Components
import { BallSelectorComponent } from './ball-selector.component';
// Vendor
import { Spectator, createComponentFactory } from '@ngneat/spectator';


describe('BallSelectorComponent', () => {
  let spectator: Spectator<BallSelectorComponent>;
  const createComponent = createComponentFactory({
    component: BallSelectorComponent,
    detectChanges: false,
    mocks: [BetService],
    imports: [],
    providers: [FormBuilder]
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should pass', () => {
    expect(true).toBeTruthy();
  });

  it('should exist', () => {
    expect(spectator.component).toBeDefined();
  });

  it('should call to clear of BetService', () => {
    const _betService = spectator.inject(BetService);
    spectator.component.clearSelection();
    expect(_betService.clear).toHaveBeenCalled();
  });

  it('should call to init of BetService', () => {
    const _betService = spectator.inject(BetService);
    spectator.component.reset();
    expect(_betService.init).toHaveBeenCalled();
  });

});
