// Angular
import { Component, OnInit } from '@angular/core';
// Services
import { BetService } from 'src/app/services/bet.service';
// Models
import { BallSelection } from 'src/app/models/ball-selection.model';
// Tools
import { FUNCTIONS } from 'src/app/tools/functions';

@Component({
  selector: 'app-ball-selector',
  templateUrl: 'ball-selector.component.html'
})
export class BallSelectorComponent implements OnInit {

  public resultMessage: string;
  public total: number;

  constructor(public betService: BetService) {
    this.resultMessage = '';
    this.total = 0;
  }

  public ngOnInit(): void {
    this.betService.getTotalEvent().subscribe((res: number) => {
      this.total = res;
    });
    this.betService.getBetDoneEvent().subscribe((res: boolean) => {
      if (res) {
        const _winningBet: BallSelection | undefined = this.betService.selectedBalls.find((item: BallSelection) => item.item === this.betService.winningBet && item.selected);
        const _bets: number = this.betService.selectedBalls.length;
        const _profit: number = this.total / _bets * 1.5;
        this.resultMessage = _winningBet ? 'you won' : 'you lost';
        this.resultMessage += this.resultMessage === 'you won' ? `: ${_profit} €` : '';
      } else {
        this.resultMessage = '';
      }
    });
    this.betService.init();
  }

  public setBallNumber(event: BallSelection): void {
    this.betService.setBallNumber(event);
  }

  public clearSelection(): void {
    this.betService.clear();
  }

  public reset(): void {
    this.betService.init();
  }

  public getClass(index: string): string {
    return FUNCTIONS.getClassByNumber(+index);
  }

}
